use std::{
    ffi::OsString,
    io::{self, BufRead},
    path::{Path, PathBuf},
    process::{Command, Output},
};

use serde::{Deserialize, Serialize};
use thiserror::Error;

#[derive(Error, Debug)]
pub enum ListGenerationsError {
    #[
        error(
            "nix-env failed: {}\nstdout:\n{}\nstderr:\n{}\n",
            .0.status,
            String::from_utf8_lossy(&.0.stdout),
            String::from_utf8_lossy(&.0.stderr)
        )
    ]
    NixEnvError(Output),

    #[error("IO error: {0}")]
    IO(#[from] io::Error),

    #[error("Invalid profile name: {0:?}")]
    InvalidProfileName(OsString),

    #[error("Invalid profile path: {0}")]
    InvalidProfilePath(PathBuf),

    #[error("Invalid nix-env output line: {0}")]
    InvalidNixEnvOutput(String),

    #[error("Invalid generation ID: {0}")]
    InvalidGenerationID(String),
}

#[derive(Serialize, Deserialize)]
pub struct Profile {
    name: Option<String>,
    generations: Vec<Generation>,
}

#[derive(Serialize, Deserialize)]
pub struct Generation {
    id: usize,
    path: PathBuf,
}

impl Profile {
    fn enumerate_generations(
        name: &str,
        profile_root: &Path,
    ) -> Result<Vec<Generation>, ListGenerationsError> {
        let out = Command::new("nix-env")
            .arg("--list-generations")
            .arg("--profile")
            .arg(profile_root.to_str().expect("Invalid path?"))
            .output()?;

        if !out.status.success() {
            return Err(ListGenerationsError::NixEnvError(out));
        }

        let mut result = vec![];

        for line in out.stdout.lines() {
            let line = line?;
            let (id, _) = line
                .trim()
                .split_once(' ')
                .ok_or_else(|| ListGenerationsError::InvalidNixEnvOutput(line.clone()))?;

            let id = id
                .parse::<usize>()
                .map_err(|_| ListGenerationsError::InvalidGenerationID(id.into()))?;
            let mut path = profile_root
                .parent()
                .ok_or_else(|| {
                    ListGenerationsError::InvalidProfilePath(profile_root.to_path_buf())
                })?
                .to_owned();

            path.push(format!("{}-{}-link", name, id));

            result.push(Generation { id, path })
        }

        Ok(result)
    }

    pub fn enumerate() -> Result<Vec<Profile>, ListGenerationsError> {
        let mut result = vec![];

        let default_profile = Path::new("/nix/var/nix/profiles/system/");

        if default_profile.is_dir() {
            result.push(Profile {
                name: None,
                generations: Self::enumerate_generations("system", default_profile)?,
            })
        }

        let system_profiles = Path::new("/nix/var/nix/profiles/system-profiles/");

        if system_profiles.is_dir() {
            for item in system_profiles.read_dir()? {
                let item = item?;

                let name = item.file_name();
                let name = name
                    .to_str()
                    .ok_or_else(|| ListGenerationsError::InvalidProfileName(name.clone()))?;

                if item.file_type()?.is_symlink() && !name.ends_with("-link") {
                    result.push(Profile {
                        name: Some(name.to_string()),
                        generations: Self::enumerate_generations(name, &item.path())?,
                    })
                }
            }
        }

        Ok(result)
    }

    pub fn name(&self) -> Option<&str> {
        self.name.as_deref()
    }

    pub fn generations(&self) -> &[Generation] {
        &self.generations
    }
}

impl Generation {
    pub fn id(&self) -> usize {
        self.id
    }
    pub fn path(&self) -> &Path {
        &self.path
    }
}
