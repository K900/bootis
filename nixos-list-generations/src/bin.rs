use nixos_list_generations::Profile;
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize)]
pub struct CliOutput {
    version: usize,
    profiles: Vec<Profile>,
}

fn main() -> anyhow::Result<()> {
    let result = serde_json::to_string_pretty(&CliOutput {
        version: 1,
        profiles: Profile::enumerate()?,
    })?;
    println!("{}", result);

    Ok(())
}
