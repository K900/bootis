use std::{path::Path, process::Command};

use tracing::instrument;

use crate::utils::CommandExt;

#[instrument("Signing and installing...", level = "trace", ret)]
pub(crate) fn sign_and_install(src: &Path, dst: &Path) -> anyhow::Result<()> {
    Command::new("sbctl")
        .arg("sign")
        .arg(src)
        .arg("--output")
        .arg(dst)
        .execute_checked()?;
    Ok(())
}
