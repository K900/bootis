use std::{
    path::{Path, PathBuf},
    process::{Command, Output},
};

use tracing::instrument;

pub(crate) trait CommandExt {
    fn execute_checked(&mut self) -> anyhow::Result<Output>;
}

impl CommandExt for Command {
    #[instrument("Running command...", ret)]
    fn execute_checked(&mut self) -> anyhow::Result<Output> {
        let output = self.output()?;

        if !output.status.success() {
            anyhow::bail!(
                "Command {:?} failed: {}\nstdout:\n{}\nstderr:\n{}\n",
                self,
                output.status,
                String::from_utf8_lossy(&output.stdout),
                String::from_utf8_lossy(&output.stdout)
            )
        }

        Ok(output)
    }
}

pub(crate) trait PathExt {
    fn with_suffix<P: AsRef<Path>>(&self, path: P) -> Self;
}

impl PathExt for PathBuf {
    fn with_suffix<P: AsRef<Path>>(&self, path: P) -> Self {
        let mut b = self.clone();
        b.push(path);
        b
    }
}
