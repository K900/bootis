use std::{
    fs::create_dir_all,
    path::{Path, PathBuf},
};

use anyhow::Context as _;
use serde::Serialize;
use tera::{Context, Tera};
use tracing::instrument;

use crate::{entries::BootEntry, sbctl::sign_and_install, utils::PathExt, Config};

#[instrument("Copying files...")]
fn ensure_installed(from: &Path, to: &Path) -> anyhow::Result<()> {
    dircpy::CopyBuilder::new(from, to).overwrite(true).run()?;

    Ok(())
}

#[instrument("Installing refind...", skip(config), ret)]
pub(crate) fn install(config: &Config) -> anyhow::Result<PathBuf> {
    let dst_dir = config.get_dst_dir();
    create_dir_all(&dst_dir).context("Creating root rEFInd directory")?;

    let source_filename = format!("refind_{}.efi", &config.arch);
    let target_filename = if config.refind.install_as_fallback {
        format!("boot{}.efi", &config.arch)
    } else {
        source_filename.clone()
    };

    let refind_src = config.refind.path.with_suffix(&source_filename);
    let refind_dst = dst_dir.with_suffix(&target_filename);

    sign_and_install(&refind_src, &refind_dst).context("Installing rEFInd")?;

    let drivers_dirname = format!("drivers_{}", &config.arch);
    let drivers_src_path = config.refind.path.with_suffix(&drivers_dirname);
    let drivers_dst_path = dst_dir.with_suffix(&drivers_dirname);
    create_dir_all(&drivers_dst_path).context("Creating drivers directory")?;

    for driver in &config.refind.drivers {
        let driver_filename = format!("{}_{}.efi", &driver, &config.arch);
        let driver_src = drivers_src_path.with_suffix(&driver_filename);
        let driver_dst = drivers_dst_path.with_suffix(&driver_filename);
        sign_and_install(&driver_src, &driver_dst)
            .with_context(|| format!("Installing driver {}", driver_filename))?;
    }

    let icons_src = config.refind.path.with_suffix("icons");
    let icons_dst = dst_dir.with_suffix("icons");
    create_dir_all(&icons_dst).with_context(|| "Creating icons directory")?;
    ensure_installed(&icons_src, &icons_dst).context("Installing icons")?;

    for ef in &config.refind.extra_files {
        ensure_installed(ef, &dst_dir)
            .with_context(|| format!("Installing extra files from {:?}", ef))?;
    }

    Ok(refind_dst.strip_prefix(&config.esp)?.to_path_buf())
}

const TEMPLATE: &str = r##"
timeout {{ config.refind.timeout }}

{% for k, v in config.refind.extra_config %}
{{ k }} {{ v }}
{% endfor %}

{% for k, v in config.refind.extra_entries %}
menuentry "{{ k }}" {
    {% for k1, v1 in v %}
    {{ k1 }} {{ v1 }}
    {% endfor %}
}
{% endfor %}

menuentry "NixOS" {
    loader /{{ bundles_dir }}/{{ entries[0].filename }}
    ostype Linux
    {% for k2, v2 in config.refind.extra_entry_config %}
    {{ k2 }} {{ v2 }}
    {% endfor %}

    {% for entry in entries %}
    submenuentry "#{{ entry.id }} - {{ entry.label }}" {
        loader /{{ bundles_dir }}/{{ entry.filename }}
        ostype Linux
        {% for k2, v2 in config.refind.extra_entry_config %}
        {{ k2 }} {{ v2 }}
        {% endfor %}
    }
    {% endfor %}
}
"##;

#[derive(Serialize)]
struct ConfigContext<'a> {
    config: &'a Config,
    entries: &'a [BootEntry],
    bundles_dir: &'a Path,
}

#[instrument("Generating config...", skip(config, entries), ret)]
pub(crate) fn render_config(
    config: &Config,
    entries: &[BootEntry],
    bundles_dir: &Path,
) -> anyhow::Result<()> {
    let mut tera = Tera::default();
    tera.add_raw_template("config", TEMPLATE)?;

    let cfg = tera
        .render(
            "config",
            &Context::from_serialize(ConfigContext {
                config,
                entries,
                bundles_dir,
            })
            .context("Preparing template")?,
        )
        .context("Rendering template")?;

    std::fs::write(config.get_dst_dir().with_suffix("refind.conf"), cfg)
        .context("Installing config")?;

    Ok(())
}
