use std::{
    cmp::Reverse,
    collections::{HashMap, HashSet},
    fs::{create_dir_all, File},
    path::PathBuf,
};

use clap::{arg, Parser};
use serde::{Deserialize, Serialize};
use tracing::info;
use tracing_subscriber::EnvFilter;

mod efibootmgr;
mod entries;
mod refind;
mod sbctl;
mod utils;

use entries::BootEntry;
use utils::PathExt;

#[derive(Deserialize, Serialize)]
struct RefindConfig {
    path: PathBuf,
    drivers: Vec<String>,
    timeout: usize,
    extra_config: HashMap<String, String>,
    extra_entries: HashMap<String, HashMap<String, String>>,
    extra_entry_config: HashMap<String, String>,
    extra_files: Vec<PathBuf>,
    install_as_fallback: bool,
}

#[derive(Deserialize, Serialize)]
struct Config {
    arch: String,
    refind: RefindConfig,
    stubs: PathBuf,
    esp: PathBuf,
    generation_limit: usize,
}

impl Config {
    fn get_dst_dir(&self) -> PathBuf {
        self.esp.with_suffix(if self.refind.install_as_fallback {
            "EFI/boot"
        } else {
            "EFI/refind"
        })
    }
}

#[derive(Parser, Debug)]
struct Args {
    #[arg(short, long, value_hint = clap::ValueHint::FilePath)]
    config: PathBuf,
}

fn main() -> anyhow::Result<()> {
    tracing_subscriber::fmt()
        .pretty()
        .with_env_filter(EnvFilter::from_default_env())
        .init();

    if whoami::username() != "root" {
        anyhow::bail!("bootis-install needs to be run as root!")
    }

    let args = Args::parse();

    let config: Config = serde_json::from_reader(File::open(args.config)?)?;

    let mut entries = BootEntry::enumerate()?;
    entries.sort_by_key(|x| Reverse(x.created));

    let refind = refind::install(&config)?;
    efibootmgr::register_if_needed(&config.esp, &refind, "rEFInd (bootis)")?;

    let base_stub = config
        .stubs
        .with_suffix(format!("linux{}.efi.stub", config.arch));

    let dest_dir = config.esp.with_suffix("EFI/nixos");
    create_dir_all(&dest_dir)?;

    let mut stub_names = HashSet::new();

    let limit = config.generation_limit.min(entries.len());
    entries.truncate(limit);

    for entry in &mut entries {
        stub_names.insert(entry.create_uki(&base_stub, &dest_dir)?);
    }

    for item in dest_dir.read_dir()? {
        let item = item?;
        let seen = item
            .file_name()
            .to_str()
            .map(|x| stub_names.contains(x))
            .unwrap_or(false);

        if !seen && item.path().is_file() {
            info!("Removing old file: {:?}", item.path());
            std::fs::remove_file(item.path())?
        }
    }

    refind::render_config(&config, &entries, dest_dir.strip_prefix(&config.esp)?)?;

    Ok(())
}
