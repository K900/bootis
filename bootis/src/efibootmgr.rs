use std::{
    fs::File,
    io::Read,
    path::{Path, PathBuf},
    process::Command,
};

use anyhow::Context;
use proc_mounts::MountIter;
use tracing::instrument;

use crate::utils::{CommandExt, PathExt};

#[derive(Debug)]
pub(crate) struct PartitionRef {
    base_disk: PathBuf,
    partition: usize,
}

impl PartitionRef {
    #[instrument("Detecting ESP disk and partition...", ret)]
    fn from_blockdev(path: &Path) -> anyhow::Result<Self> {
        let basename = path
            .file_name()
            .context("ESP backing device is not a file!")?;
        let blockdev = PathBuf::from("/sys/class/block")
            .with_suffix(basename)
            .canonicalize()?;

        let base_dev = blockdev
            .parent()
            .context("ESP backing device has no disk?")?
            .file_name()
            .context("ESP backing device has no name?")?;
        let base_dev_path = PathBuf::from("/dev").with_suffix(base_dev);

        let mut buf = vec![];
        File::open(blockdev.with_suffix("partition"))?.read_to_end(&mut buf)?;

        let part_number = String::from_utf8_lossy(&buf).trim().parse::<usize>()?;

        Ok(PartitionRef {
            base_disk: base_dev_path,
            partition: part_number,
        })
    }

    #[instrument("Detecting ESP device...", ret)]
    fn find_esp(esp: &Path) -> anyhow::Result<Self> {
        let esp_mount = MountIter::new()?
            .flatten()
            .find(|x| x.dest == esp)
            .context("Failed to find ESP mount point!")?;
        Self::from_blockdev(&esp_mount.source)
    }
}

#[instrument("Checking for existing boot entry registration...", ret)]
pub(crate) fn is_registered() -> anyhow::Result<bool> {
    let out = Command::new("efibootmgr").execute_checked()?;

    Ok(String::from_utf8_lossy(&out.stdout).contains("bootis"))
}

#[instrument("Registering refind...", ret)]
pub(crate) fn register_if_needed(esp: &Path, loader: &Path, label: &str) -> anyhow::Result<()> {
    if is_registered()? {
        // FIXME: this is ass
        return Ok(());
    }

    let esp = PartitionRef::find_esp(esp)?;

    Command::new("efibootmgr")
        .arg("--create")
        .arg("--disk")
        .arg(esp.base_disk)
        .arg("--part")
        .arg(esp.partition.to_string())
        .arg("--loader")
        .arg(loader)
        .arg("--label")
        .arg(label)
        .execute_checked()?;

    Ok(())
}
