use std::{
    collections::HashMap,
    fs::File,
    io::Write,
    os::unix::prelude::OsStrExt,
    path::{Path, PathBuf},
    process::Command,
    time::SystemTime,
};

use anyhow::Context;
use bootspec::{generation::Generation, BootJson};
use md5::{Digest, Md5};
use nixos_list_generations::Profile;
use serde::{Deserialize, Serialize};
use tracing::warn;

use crate::{
    sbctl,
    utils::{CommandExt, PathExt},
};

fn read_spec(path: &Path) -> anyhow::Result<BootJson> {
    let mut spec_path = path.to_path_buf();
    spec_path.push("bootspec/boot.v1.json");

    if spec_path.is_file() {
        Ok(serde_json::from_reader(File::open(spec_path)?)?)
    } else {
        Ok(BootJson::synthesize_version(path, 1).map_err(|e| anyhow::anyhow!(e))?)
    }
}

#[derive(Debug, Deserialize, Serialize)]
pub(crate) struct BootEntry {
    pub created: SystemTime,
    pub profile: Option<String>,
    pub id: usize,
    pub specialization: Option<String>,
    pub label: String,
    pub kernel: PathBuf,
    pub initrd: Option<PathBuf>,
    pub init: PathBuf,
    pub cmdline: Vec<String>,
    pub filename: Option<String>,
    pub os_release: PathBuf,
}

impl BootEntry {
    pub(crate) fn enumerate() -> anyhow::Result<Vec<Self>> {
        let profiles = Profile::enumerate()?;
        let mut entries = vec![];

        for profile in profiles {
            let name = profile.name().map(|x| x.to_string());

            for generation in profile.generations() {
                let created = generation.path().symlink_metadata()?.created()?;
                let Generation::V1(mut spec) = read_spec(generation.path())?.generation else {
                    warn!("Unsupported generation: {:?}", generation.path());
                    continue;
                };
                let specializations = spec.specialisations;
                let os_release = generation.path().join("etc/os-release");
                spec.specialisations = HashMap::new();

                entries.push(BootEntry {
                    created,
                    profile: name.clone(),
                    id: generation.id(),
                    specialization: None,
                    label: spec.bootspec.label,
                    kernel: spec.bootspec.kernel,
                    initrd: spec.bootspec.initrd,
                    init: spec.bootspec.init,
                    cmdline: spec.bootspec.kernel_params,
                    os_release: os_release.clone(),
                    filename: None,
                });

                for (k, v) in specializations {
                    entries.push(BootEntry {
                        created,
                        profile: name.clone(),
                        id: generation.id(),
                        specialization: Some(k.0.to_string()),
                        label: v.bootspec.label,
                        kernel: v.bootspec.kernel,
                        initrd: v.bootspec.initrd,
                        init: v.bootspec.init,
                        cmdline: v.bootspec.kernel_params,
                        os_release: os_release.clone(),
                        filename: None,
                    })
                }
            }
        }

        Ok(entries)
    }

    fn hash(&self) -> anyhow::Result<String> {
        let mut hasher = Md5::default();
        hasher.update(self.kernel.as_os_str().as_bytes());
        hasher.update(b"\x00");
        hasher.update(self.full_cmdline()?);
        hasher.update(b"\x00");
        hasher.update(
            self.initrd
                .as_ref()
                .map(|x| x.as_os_str().as_bytes())
                .unwrap_or(&[]),
        );
        hasher.update(b"\x00");
        Ok(base16ct::lower::encode_string(&hasher.finalize()))
    }

    fn full_cmdline(&self) -> anyhow::Result<String> {
        let mut result = Vec::new();
        result.push(format!(
            "init={}",
            self.init
                .to_str()
                .context("Init path contains special characters?")?
        ));
        result.extend(self.cmdline.iter().cloned());

        Ok(result.join(" "))
    }

    pub(crate) fn create_uki(&mut self, stub: &Path, dest_dir: &Path) -> anyhow::Result<String> {
        let mut cmdline = tempfile::NamedTempFile::new()?;
        writeln!(cmdline, "{}", self.full_cmdline()?)?;

        let output = tempfile::NamedTempFile::new()?;

        let filename = format!("{}.efi", self.hash()?);
        let dst = dest_dir.to_path_buf().with_suffix(&filename);

        let mut cmd = Command::new("sbctl");

        cmd.arg("bundle")
            .arg(output.path())
            .arg("--efi-stub")
            .arg(stub)
            .arg("--kernel-img")
            .arg(&self.kernel)
            .arg("--cmdline")
            .arg(cmdline.path())
            .arg("--os-release")
            .arg(&self.os_release);

        if let Some(ref initrd) = self.initrd {
            cmd.arg("--initramfs").arg(initrd);
        };

        cmd.execute_checked()?;

        sbctl::sign_and_install(output.path(), &dst)?;

        self.filename = Some(filename.clone());

        Ok(filename)
    }
}
