{
  description = "Bootloader experimentation";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable-small";
    flake-utils.url = "github:numtide/flake-utils";
    pre-commit-hooks = {
      url = "github:cachix/pre-commit-hooks.nix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs =
    {
      self,
      nixpkgs,
      flake-utils,
      pre-commit-hooks,
    }:
    (flake-utils.lib.eachDefaultSystem (
      system:
      let
        pkgs = nixpkgs.legacyPackages.${system};
      in
      {
        packages = rec {
          bootis = pkgs.callPackage ./nix/bootis.nix { };
          default = bootis;
        };

        devShells.default = pkgs.mkShell {
          RUST_SRC_PATH = pkgs.rustPlatform.rustLibSrc;

          buildInputs = with pkgs; [
            sbctl
            refind
            efibootmgr
            rustc
            cargo
            rustfmt
            clippy
            cargo-outdated
          ];

          inherit (self.checks.${system}.pre-commit-check) shellHook;
        };

        checks.pre-commit-check = pre-commit-hooks.lib.${system}.run {
          src = ./.;
          hooks = {
            nixfmt-rfc-style.enable = true;
            deadnix.enable = true;
            statix.enable = true;

            rustfmt.enable = true;
            clippy.enable = true;
            cargo-check.enable = true;
          };
        };
      }
    ))
    // {
      nixosModules = {
        default = import ./nix/bootis-nixos.nix;
        theme-regular = import ./nix/theme-regular.nix;
      };
    };
}
