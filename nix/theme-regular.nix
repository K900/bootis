{
  pkgs,
  config,
  lib,
  ...
}:
let
  cfg = config.boot.loader.bootis.refind.theme.regular;

  themePackage = pkgs.stdenvNoCC.mkDerivation {
    pname = "refind-theme-regular";
    version = "unstable-2022-09-10";

    src = pkgs.fetchFromGitHub {
      owner = "bobafetthotmail";
      repo = "refind-theme-regular";
      rev = "1ab545c54516a64ab95285a85d010db0bbd38b6b";
      hash = "sha256-2T15ZswdUsqZmZTzBwEVml7VJYIgFQVnpsWVPd8+ZVc=";
    };

    buildCommand = ''
      mkdir -p $out/themes/regular
      cp -r --no-preserve=mode $src/{icons,fonts} $out/themes/regular

      # use objectively better NixOS icon
      for d in $out/themes/regular/icons/*; do
        mv $d/os_nixos-alt.png $d/os_nixos.png
      done
    '';
  };
in
{
  options = {
    boot.loader.bootis.refind.theme.regular = with lib; {
      enable = mkEnableOption "bootis bootloader - rEFInd Regular theme";
      color = mkOption {
        description = "Color mode - light or dark";
        type = types.enum [
          "light"
          "dark"
        ];
        default = "light";
      };
      size = mkOption {
        description = "Icon size";
        type = types.enum [
          "s"
          "m"
          "l"
          "xl"
        ];
        default = "m";
      };
      font.name = mkOption {
        description = "Font name";
        type = types.enum [
          "source-code-pro-extralight"
          "nimbus-mono"
          "ubuntu-mono"
        ];
        default = "source-code-pro-extralight";
      };
      font.size = mkOption {
        description = "Font size";
        type = types.enum [
          14
          16
          18
          28
          30
          32
          42
          44
          46
        ];
        default = 18;
      };
    };
  };

  config = lib.mkIf cfg.enable {
    boot.loader.bootis = {
      extraFiles = [ themePackage ];
      extraConfig =
        let
          smallIconSizes = {
            s = 48;
            m = 96;
            l = 144;
            xl = 192;
          };
          largeIconSizes = {
            s = 128;
            m = 256;
            l = 384;
            xl = 512;
          };
          smallIconSize = toString smallIconSizes.${cfg.size};
          largeIconSize = toString largeIconSizes.${cfg.size};
          directory = "${largeIconSize}-${smallIconSize}";

          colorSuffixes = {
            dark = "_dark";
            light = "";
          };
          colorSuffix = colorSuffixes.${cfg.color};
        in
        rec {
          icons_dir = "themes/regular/icons/${directory}";
          small_icon_size = smallIconSize;
          big_icon_size = largeIconSize;
          banner = "${icons_dir}/bg${colorSuffix}.png";
          selection_small = "${icons_dir}/selection${colorSuffix}-small.png";
          selection_big = "${icons_dir}/selection${colorSuffix}-big.png";
          font = "themes/regular/fonts/${cfg.font.name}-${toString cfg.font.size}.png";
        };
    };
  };
}
