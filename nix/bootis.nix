{ rustPlatform }:
rustPlatform.buildRustPackage {
  pname = "bootis";
  version = "0.0.1";

  src = ./..;
  buildAndTestSubdir = "bootis";

  cargoLock.lockFile = ../Cargo.lock;

  postPatch = ''
    cp ${../Cargo.lock} Cargo.lock
  '';
}
