{
  config,
  lib,
  pkgs,
  ...
}:
let
  cfg = config.boot.loader;
  bcfg = cfg.bootis;
  bootis = pkgs.callPackage ./bootis.nix { };

  inherit (config.nixpkgs) system;

  archSuffix =
    if system == "x86_64-linux" then
      "x64"
    else if system == "aarch64-linux" then
      "aa64"
    else
      throw "Unsupported system: ${system}";

  refindConfigType =
    with lib;
    types.attrsOf (
      types.coercedTo
        # coercedType
        (types.listOf types.str)
        # coerceFunc
        (strings.concatStringsSep ",")
        # finalType
        types.str
    );
in
{
  options = {
    boot.loader.bootis = with lib; {
      enable = mkEnableOption "bootis bootloader";
      drivers = mkOption {
        description = "Filesystem drivers to install";
        type = types.listOf types.str;
        default = [
          "btrfs"
          "ext4"
        ];
      };
      generationLimit = mkOption {
        description = "Maximum number of generations to preserve, or `null` to keep all.";
        type = types.nullOr types.int;
        default = 5;
      };
      extraConfig = mkOption {
        description = "Extra configuration for refind.conf";
        type = refindConfigType;
        default = { };
      };
      extraEntries = mkOption {
        description = "Extra entries to add to refind.conf";
        type = types.attrsOf refindConfigType;
        default = { };
      };
      extraEntryConfig = mkOption {
        description = "Extra per-entry parameters to add to NixOS menu entries";
        type = refindConfigType;
        default = { };
      };
      extraFiles = mkOption {
        description = "Extra files to copy to the rEFInd directory";
        type = types.listOf types.package;
        default = [ ];
      };
      logLevel = mkOption {
        description = "Log level for bootis installer - mostly used for development";
        type = types.str;
        default = "info";
      };
      installAsFallback = mkOption {
        description = "Install rEFInd to fallback path (EFI/boot/boot{arch}.efi)";
        type = types.bool;
        default = false;
      };
    };
  };
  config = lib.mkIf bcfg.enable {
    environment.systemPackages = [ pkgs.sbctl ];

    boot.loader.external = {
      enable = true;
      installHook =
        let
          configFile = pkgs.writeTextFile {
            name = "bootis-config";
            text = builtins.toJSON {
              arch = archSuffix;
              refind = {
                path = "${pkgs.refind}/share/refind";
                inherit (bcfg) drivers;
                inherit (cfg) timeout;
                extra_config = bcfg.extraConfig;
                extra_entries = bcfg.extraEntries;
                extra_entry_config = bcfg.extraEntryConfig;
                extra_files = map toString bcfg.extraFiles;
                install_as_fallback = bcfg.installAsFallback;
              };
              stubs = "${pkgs.systemd}/lib/systemd/boot/efi";
              esp = cfg.efi.efiSysMountPoint;
              generation_limit = bcfg.generationLimit;
            };
          };

          script = pkgs.writeShellScript "bootis-install" ''
            export PATH=${
              lib.makeBinPath [
                pkgs.sbctl
                pkgs.binutils-unwrapped
                pkgs.efibootmgr
                config.nix.package
              ]
            }
            export RUST_LOG=${bcfg.logLevel}
            ${bootis}/bin/bootis --config=${configFile}
          '';
        in
        script;
    };
  };
}
