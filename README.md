# bootis

![](./pyro.jpg)

**Do not use this.**

Seriously, just don't.

You will regret it.

## But I wanna

Then steel yourself, and grab a drink - this may take a while.

Before you do anything, ensure you're on a recent enough NixOS unstable version (at least around mid-December 2022).

Now, get into a shell with `sbctl` (with either `nix-shell -p sbctl` or `nix shell nixpkgs#sbctl`, depending on your preferred kind of Nix CLI) and run `sbctl create-keys`.
This will generate the Secure Boot keys and stash them in `/etc/secureboot`.
Now is as good of a time as any to make a backup of that directory. **Make one. You're going to need it.**

Next, add the module from the flake to your config, set `boot.loader.bootis.enable = true;`, rebuild and reboot.
If the activation fails, you probably didn't generate the keys, or didn't generate them correctly.

If everything is good, reboot into your firmware, switch Secure Boot to setup mode, go back to NixOS and run `sbctl enroll-keys` to actually register your keys and disable booting unsigned binaries.

(Note: if it yells at you about option ROMs, that's great, you just saved yourself from hosing the machine. Do what the error message tells you and pray.)

Now, cross your fingers and reboot one last time. If it boots, you should be good! Enjoy your sandwich.

## What works

* The basics: signing and installing rEFInd and kernel
* Very barebones deduplication of bundles to (attempt to) save space

## What doesn't

* Not creating duplicate boot entries is very janky
* Error recovery doesn't exist
* Secure boot is always on
* It will probably explode if you try it on another computer
